package com.kapango.jobstash.client.meaningcloud;

import com.kapango.jobstash.config.MeaningCloudConfig;
import com.meaningcloud.*;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class MeaningCloudClient {

    private static final Request.Language LANG = Request.Language.EN;

    private final String apiKey;

    public MeaningCloudClient(final MeaningCloudConfig config) {
        this.apiKey = config.getApiKey();
    }


    public ParserResponse parseText(final String text) throws IOException, Request.ParameterValidationException {
        return ParserRequest
                .build(apiKey, LANG)
                .withText(text)
                .send();
    }

    public TopicsResponse parseTextForTopics(final String text) throws Request.ParameterValidationException, IOException {
        return TopicsRequest
                .build(apiKey, LANG)
                .withText(text)
                .send();
    }

}
