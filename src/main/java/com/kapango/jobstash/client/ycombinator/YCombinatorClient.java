package com.kapango.jobstash.client.ycombinator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;
import java.util.stream.Collectors;

import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * Fetches YCombinator information by scraping its public html page
 */
@Component
@Slf4j(topic = "YCOMBINATOR_CLIENT")
public class YCombinatorClient {

    private String imagePath = "img/organisation/";
    private File companyImageDirectory = new File(imagePath);

    public YCombinatorClient() {
        companyImageDirectory.mkdirs();
    }

    public YCombinatorCompanyResponse fetchCompanyByUrl(final String url) throws IOException {
        var doc = Jsoup.connect(url).get();
        return buildResponse(doc);
    }

    public YCombinatorCompanyResponse fetchCompanyByName(final String company) throws IOException {
        var ycombinatorUrl = "https://www.ycombinator.com/companies/" + company;
        var doc = Jsoup.connect(ycombinatorUrl).get();
        return buildResponse(doc);
    }

    private YCombinatorCompanyResponse buildResponse(Document doc) throws IOException {
        var companyName = doc.select("h1").text();
        var builder = YCombinatorCompanyResponse.builder()
                .companyName(companyName)
                .companyUrl(doc.select("div.links:nth-child(4) > a:nth-child(2)").text())
                .descriptionShort(doc.select("div.main-column:nth-child(2) > div:nth-child(2) > h3:nth-child(2)").text())
                .descriptionLong(doc.select("p.pre-line:nth-child(3)").text())
                .stage(doc.select(".main-column > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > span:nth-child(1)").text());


        try {
            builder.employees(Integer.valueOf(doc.select(".facts > div:nth-child(2) > span:nth-child(1)").text()));
        } catch (Exception e) {
            log.warn("Unable to get Employee value from YCombinator for {}", companyName);
        }
        try {
            builder.location(doc.select(".facts > div:nth-child(3) > span:nth-child(1)").text());
        } catch (Exception e) {
            log.warn("Unable to get Location value from YCombinator for {}", companyName);
        }
        try {
            builder.yearFounded(Integer.valueOf(doc.select(".facts > div:nth-child(1) > span:nth-child(1)").text()));
        } catch (Exception e) {
            log.warn("Unable to get Year founded value from YCombinator for {}", companyName);
        }

        try {
            builder.ycombinatorIntake(doc.select(".pill-orange").text());
        } catch (Exception e) {
            log.warn("Unable to get Year founded value from YCombinator for {}", companyName);
        }

        Elements linkedIn = doc.select("a.bg-image-linkedin:nth-child(1)");
        Elements twitter = doc.select("a.bg-image-twitter:nth-child(2)");
        Elements facebook = doc.select("a.social:nth-child(3)");
        Elements crunchbase = doc.select("a.social:nth-child(4)");

        Elements sectors = doc.select("div.main-column:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2)");
        builder.sectors(sectors.select(".pill").stream().map(Element::text).collect(Collectors.toList()));

        Elements imageAlt = doc.select(".round-logo-wrapper > img:nth-child(1)");
        Elements image = doc.select(".company-logo");

        UUID uuid = UUID.randomUUID();

        if (!image.isEmpty() && !image.attr("src").isEmpty()) {
            try {
                Connection.Response resultImageResponse = Jsoup.connect(image.attr("src"))
                        .ignoreContentType(true)
                        .execute();
                File imageFile = new File(imagePath + uuid + ".png");
                try (FileOutputStream outputStream = new FileOutputStream(imageFile)) {
                    outputStream.write(resultImageResponse.bodyAsBytes());
                }

                builder.image(imageFile.getName());

            } catch (HttpStatusException e) {
                log.warn("Unable to get image for {} , HTTP status code {}", companyName, e.getStatusCode());
            }
        }

        if (!imageAlt.isEmpty() && !imageAlt.attr("src").isEmpty()) {

            try {
                Connection.Response resultImageResponse = Jsoup.connect(imageAlt.attr("src"))
                        .ignoreContentType(true)
                        .execute();

                File imageFile = new File(imagePath + uuid + "_alt" + ".png");
                try (FileOutputStream outputStream = new FileOutputStream(imageFile)) {
                    outputStream.write(resultImageResponse.bodyAsBytes());
                }

                builder.imageAlt(imageFile.getName());
            } catch (HttpStatusException e) {
                log.warn("Unable to get Alt Image for {} , HTTP status code {}", companyName, e.getStatusCode());
            }
        }

        if (!linkedIn.isEmpty()) {
            builder.linkedInUrl(linkedIn.attr("href"));
        }
        if (!twitter.isEmpty()) {
            builder.twitterUrl(twitter.attr("href"));

        }
        if (!facebook.isEmpty()) {
            builder.facebookUrl(facebook.attr("href"));

        }
        if (!crunchbase.isEmpty()) {
            builder.crunchbaseUrl(crunchbase.attr("href"));
        }

        return builder.build();
    }
}
