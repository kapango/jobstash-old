package com.kapango.jobstash.client.glassdoor;

public class GlassDoorClientException extends Exception {

    public GlassDoorClientException() {
    }

    public GlassDoorClientException(String message) {
        super(message);
    }

    public GlassDoorClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public GlassDoorClientException(Throwable cause) {
        super(cause);
    }

    public GlassDoorClientException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
