package com.kapango.jobstash.client.glassdoor;

import java.io.IOException;
import java.net.URI;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriTemplate;

import lombok.extern.slf4j.Slf4j;


@Component
@Slf4j(topic = "GLASSDOOR_CLIENT")
public class GlassdoorHttpClientImpl implements GlassdoorHttpClient {

    private static final String GD_URL = "https://www.glassdoor.co.uk/Reviews/{company}-reviews-SRCH_KE0,{length}.htm";

    public GlassdoorHttpClientImpl() {}

    public GlassdoorCompanyResponse getGlassdoorCompany(final String company) throws GlassDoorNotFoundException, GlassDoorClientException {
        return getGlassdoorCompany(company, company.length());
    }

    @Override
    public GlassdoorCompanyResponse getGlassdoorCompany(final String company, final Integer length) throws GlassDoorClientException,
            GlassDoorNotFoundException {

        try {
            final String gdCompany = company.replace(" ", "-");
            final URI uri = new UriTemplate(GD_URL).expand(gdCompany, length);

            log.info("Fetching company details from URL: {}", uri);

            final Document response = Jsoup.connect(uri.toString())
                    .userAgent("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0")
                    .referrer("http://www.google.co.uk")
                    .get();

            return GlassdoorParser.extractFromPage(response);

        } catch (IOException e) {
            throw new GlassDoorClientException(e);
        }
    }
}
