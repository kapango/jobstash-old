package com.kapango.jobstash.client.glassdoor;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

public interface GlassdoorHttpClient {


    /**
     * Tries to find a glassdoor page
     * @param company string
     * @param length integer
     * @return Document
     */
    @GET
    @Path("Reviews/{company}-reviews-SRCH_KE0,{length}.htm")
    @Produces({MediaType.TEXT_HTML})
    @Consumes(MediaType.TEXT_HTML)
    GlassdoorCompanyResponse getGlassdoorCompany(@PathParam("company")String company, @PathParam("length") Integer length) throws GlassDoorClientException, GlassDoorNotFoundException;


}
