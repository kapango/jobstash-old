package com.kapango.jobstash.client.glassdoor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GlassdoorCompanyResponse {

    private String name;
    private String url;
    private String image;
    private Double score;

}
