package com.kapango.jobstash.client.glassdoor;

public class GlassDoorNotFoundException extends Throwable {
    public GlassDoorNotFoundException() {
    }

    public GlassDoorNotFoundException(String message) {
        super(message);
    }

    public GlassDoorNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public GlassDoorNotFoundException(Throwable cause) {
        super(cause);
    }

    public GlassDoorNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
