package com.kapango.jobstash.client.glassdoor;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import lombok.extern.slf4j.Slf4j;

/**
 * Discombubulates a Glassdoor page to extract the tasty data
 *
 * Yes, discombobulate is just a random word I used
 */
@Slf4j(topic = "GLASSDOOR_PARSER")
public class GlassdoorParser {

    private static final String REVIEW_PAGE = "Reviews";
    private static final String COMPANY_PAGE = "Overview";

    public static GlassdoorCompanyResponse extractFromPage(final Document page) throws GlassDoorNotFoundException {

        //glassdoor is smart in that it redirects you if there is only one result for a company name
        if (page.location().contains(REVIEW_PAGE)) {


            //get first element in list of eiHdrModule

            final Elements results = page.getElementsByClass("eiHdrModule");

            if (!results.isEmpty()) {

                final Element firstResult = results.get(0);
                final Element employerInfo = firstResult.getElementsByClass("empInfo").get(0);
                final Element employerLinks = firstResult.getElementsByClass("empLinks").get(0);

                final String score = employerLinks.getElementsByClass("bigRating").first().text();
                final String employerName = employerInfo.getElementsByClass("tightAll h2").first().text();
                final String url = employerInfo.getElementsByClass("tightAll h2").first().attr("href");


                return GlassdoorCompanyResponse.builder()
                        .name(employerName)
                        .url("https://glassdoor.com/" + url)
                        .score(Double.valueOf(score))
                        .build();


            } else {
                throw new GlassDoorNotFoundException("Could not find company");
            }

        } else if (page.location().contains(COMPANY_PAGE)) {
            final String companyName = page.select("#DivisionsDropdownComponent").text();

            Double score = null;
            try {
                score = Double.valueOf(page.select(".css-1c86vvj").text());
            } catch (Exception e) {
                log.warn("No ratings exist for {}", companyName);
            }

            return GlassdoorCompanyResponse.builder()
                    .name(companyName)
                    .url(page.location())
                    .score(score)
                    .build();

        } else {
            throw new GlassDoorNotFoundException("Not a recognizable page : " + page.location());
        }
    }
}
