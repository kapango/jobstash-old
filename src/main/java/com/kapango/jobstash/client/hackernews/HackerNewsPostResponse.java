package com.kapango.jobstash.client.hackernews;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * Defines all meta data relating to a Hacker News post
 */
@Data
@NoArgsConstructor
public class HackerNewsPostResponse {

  private String by;
  private Integer descendants;
  private Integer id;
  private Integer parent;
  private Set<String> kids;
  private Integer score;
  private Integer time;
  private String title;
  private String type;
  private String url;
  private String text;
  private boolean deleted;
  private boolean dead;

}
