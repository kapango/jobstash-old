package com.kapango.jobstash.client.hackernews;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class HackerNewsUserResponse {

  private String about;
  private Integer created;
  private String id;
  private Integer karma;
  private List<Integer> submitted;

}
