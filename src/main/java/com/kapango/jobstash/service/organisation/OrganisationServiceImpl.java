package com.kapango.jobstash.service.organisation;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.kapango.jobstash.model.organisation.Organisation;
import com.kapango.jobstash.repository.organisation.OrganisationRepository;

@Service
public class OrganisationServiceImpl implements OrganisationService {

    private final OrganisationRepository repository;

    public OrganisationServiceImpl(final OrganisationRepository repository) {
        this.repository = repository;
    }

    @Override
    public Organisation getOrganisationByName(final String name) throws OrganisationNotFoundException {

        final Optional<Organisation> organisation = repository.findOrganisationByName(name);

        if (organisation.isPresent()) {
            return organisation.get();
        } else {
            throw new OrganisationNotFoundException("No Organisation found with the name : " + name);
        }
    }

    @Override
    public Integer createOrganisation(final Organisation organisation) {
        return repository.save(organisation).getId();
    }

    @Override
    public Organisation createIfNotExists(final String name) throws OrganisationServiceException {

        final Optional<Organisation> organisation = repository.findOrganisationByName(name);

        return organisation.orElseGet(() -> repository.save(Organisation.builder()
                .name(name)
                .created(LocalDateTime.now())
                .build()));
    }

    @Override
    public Organisation save(Organisation organisation) {
        return repository.save(organisation);
    }

    @Override
    public List<Organisation> findAll() {
        return repository.findAll();
    }

    @Override
    public Optional<Organisation> findById(Integer id) {
        return repository.findById(id);
    }
}
