package com.kapango.jobstash.service.organisation;

import java.util.List;
import java.util.Optional;

import com.kapango.jobstash.model.organisation.Organisation;

public interface OrganisationService {

    Organisation getOrganisationByName(final String name) throws OrganisationNotFoundException;

    Integer createOrganisation(final Organisation organisation);

    Organisation createIfNotExists(final String name) throws OrganisationServiceException;

    Organisation save(final Organisation organisation);

    List<Organisation> findAll();

    Optional<Organisation> findById(final Integer id);
}
