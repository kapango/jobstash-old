package com.kapango.jobstash.service.organisation;

public class OrganisationServiceException extends Exception {

    public OrganisationServiceException(final String message) {
        super(message);
    }
}
