package com.kapango.jobstash.service.organisation;

public class OrganisationNotFoundException extends Exception {

    public OrganisationNotFoundException(final String message) {
        super(message);
    }
}
