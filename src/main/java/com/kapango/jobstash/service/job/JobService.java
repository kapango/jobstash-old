package com.kapango.jobstash.service.job;

import com.kapango.jobstash.api.job.JobApi;
import com.kapango.jobstash.model.job.Job;

/**
 * Job Service must at minimum support the public Job Api
 */
public interface JobService extends JobApi {
    Job save(Job job);
}
