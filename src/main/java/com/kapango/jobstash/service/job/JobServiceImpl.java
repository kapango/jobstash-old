package com.kapango.jobstash.service.job;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kapango.jobstash.api.job.JobResponse;
import com.kapango.jobstash.client.hackernews.HackerNewsHttpClientImpl;
import com.kapango.jobstash.model.job.Job;
import com.kapango.jobstash.repository.job.JobRepository;

@Service
public class JobServiceImpl implements JobService {

    private final HackerNewsHttpClientImpl client;
    private final JobRepository jobRepository;

    @Autowired
    public JobServiceImpl(final HackerNewsHttpClientImpl client,
                          final JobRepository jobRepository) {
        this.client = client;
        this.jobRepository = jobRepository;
    }

    @Override
    public List<String> getLatestJobIds() {
        return client.getLatestJobs();
    }

    @Override
    public JobResponse getJobDetails(final String jobId) {
        return JobMapper.map(client.getPostDetails(jobId));
    }

    @Override
    public Job save(final Job job) {
        return jobRepository.save(job);
    }

}
