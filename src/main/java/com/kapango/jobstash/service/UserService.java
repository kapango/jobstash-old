package com.kapango.jobstash.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kapango.jobstash.model.user.JobstashUser;
import com.kapango.jobstash.repository.user.UserRepository;

@Service
public class UserService {

    private final UserRepository repository;

    public UserService(@Autowired final UserRepository repository) {
        this.repository = repository;
    }

    public Optional<JobstashUser> get(final Integer id) {
        return repository.findById(id);
    }

    public JobstashUser findUserByEmail(final String email) throws UserNotFoundException {

        Optional<JobstashUser> firstByEmail = repository.findFirstByEmail(email);

        if (firstByEmail.isPresent()) {
            return firstByEmail.get();
        } else {
            throw new UserNotFoundException("Could not find username with email : " + email);
        }
    }

    public JobstashUser update(JobstashUser entity) {
        return repository.save(entity);
    }

    public void delete(Integer id) {
        repository.deleteById(id);
    }

    public int count() {
        return (int) repository.count();
    }

}
