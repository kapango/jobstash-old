package com.kapango.jobstash.service;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.SimpleTokenizer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

@Service
public class NLPService {

    public static final String NOUN_PROPER = "NNP";
    public static final String NOUN = "NN";
    public static final String ADJECTIVE = "JJ";
    public static final String VERB = "VB";


    private final String SENTENCE_DICTIONARY = "/en-sent.bin";
    private final String TOKENS_MODEL = "/en-pos-maxent.bin";
    private final SentenceDetectorME sdetector;
    private final POSTaggerME posTaggerME;

    public NLPService() throws IOException {

        final SentenceModel model = new SentenceModel(getClass().getResourceAsStream(SENTENCE_DICTIONARY));
        final POSModel posModel = new POSModel(getClass().getResourceAsStream(TOKENS_MODEL));
        sdetector = new SentenceDetectorME(model);
        posTaggerME = new POSTaggerME(posModel);
    }

    public List<String> getSentencesFromText(final String text) {
        return Arrays.asList(sdetector.sentDetect(text));
    }

    public List<String> getTokensFromText(final String text) {
        return Arrays.asList(SimpleTokenizer.INSTANCE.tokenize(text));
    }

    public Map<String, Integer> getTokensFromSentences(final List<String> sentences) {

        final Map<String, Integer> wordCounts = new HashMap<>();

        sentences.forEach(sentence -> getTokensFromText(sentence).forEach(token -> {

            if (wordCounts.containsKey(token)) {
                wordCounts.put(token, wordCounts.get(token) + 1);
            } else {
                wordCounts.put(token, 1);
            }

        }));

        return wordCounts;
    }

    public Map<String, Integer> getTokensOfTypeFromSentences(final List<String> sentences, final List<String> tags) {

        final Map<String, Integer> wordCounts = new HashMap<>();

        sentences.forEach(sentence -> getTokensFromText(sentence).forEach(token -> {

            if (tags.contains(getTagForToken(token))) {
                if (wordCounts.containsKey(token)) {
                    wordCounts.put(token, wordCounts.get(token) + 1);
                } else {
                    wordCounts.put(token, 1);
                }
            }
        }));

        return wordCounts;
    }

    public String getTagForToken(final String token) {
        return posTaggerME.tag(new String[]{token})[0];
    }

    public List<String> getNounsFromTokensList(final List<String> tokens) {
        return tokens.stream()
                .filter(StringUtils::isAlphanumeric)
                .filter(token -> {
                    final String tag = getTagForToken(token);
                    return tag.equals("NNP") || tag.equals("NN");
                })
                .collect(Collectors.toList());
    }

    public Map<String, Integer> sortTokensMap(final Map<String, Integer> tokensMap) {

        return tokensMap
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(
                        toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                                LinkedHashMap::new));
    }

}
