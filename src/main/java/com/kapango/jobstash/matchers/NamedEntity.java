package com.kapango.jobstash.matchers;

import java.util.List;
import java.util.Map;

public abstract class NamedEntity {

    public static String PRIMARY = "primary";
    public static String SECONDARY = "secondary";

    /**
     * Iterates through a list of words and detects entities of the type
     * @param words
     * @return
     */
    public abstract Map<String, String> detect(final List<String> words);

}
