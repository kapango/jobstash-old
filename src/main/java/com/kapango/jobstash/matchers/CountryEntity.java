package com.kapango.jobstash.matchers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Component
public class CountryEntity extends NamedEntity {

    private final HashMap<String, String> iso2Map;
    private final List<Country> countryList;
    private final HashMap<String, String> iso3Map;
    private final HashSet<String> countries;

    public CountryEntity(final ObjectMapper mapper) throws URISyntaxException, IOException {

        final URL programmingLanguagesUrl = getClass().getClassLoader().getResource("json/entities/countries.json");
        iso2Map = new HashMap<>();
        iso3Map = new HashMap<>();
        countries = new HashSet<>();

        countryList
                = mapper.readValue(new File(programmingLanguagesUrl.toURI()), new TypeReference<List<Country>>() {
        });

        countryList.forEach(country -> {

            countries.add(country.getName());
            iso2Map.put(country.getIso2(), country.getName());
            iso3Map.put(country.getIso3(), country.getName());
        });
    }

    @Override
    public Map<String, String> detect(final List<String> words) {
        return null;
    }
}
