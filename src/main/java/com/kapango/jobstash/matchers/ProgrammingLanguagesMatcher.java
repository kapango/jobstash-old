package com.kapango.jobstash.matchers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.stereotype.Component;

import com.kapango.jobstash.model.token.ProgrammingLanguage;
import com.kapango.jobstash.repository.token.ProgrammingLanguageRepository;

@Component
public class ProgrammingLanguagesMatcher implements Matcher<ProgrammingLanguage> {

    private final ProgrammingLanguageRepository repository;

    public ProgrammingLanguagesMatcher(final ProgrammingLanguageRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<ProgrammingLanguage> match(List<String> words) {

        var programmingLanguages = repository.findAll();
        var matches = new HashSet<ProgrammingLanguage>();

        words.forEach(word -> programmingLanguages.forEach(pl -> {
            if (word.equalsIgnoreCase(pl.getName()) || word.equalsIgnoreCase(pl.getAlias())) {
                matches.add(pl);
            }
        }));

        return new ArrayList<>(matches);
    }
}
