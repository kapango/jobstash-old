package com.kapango.jobstash.matchers;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class ProgrammingTools extends NamedEntity {

    private final Set<String> programmingTools;
    private final ObjectMapper mapper;

    public ProgrammingTools(final ObjectMapper mapper) throws IOException, URISyntaxException {
        final URL programmingToolsUrl = getClass().getClassLoader().getResource("json/entities/programming_tools.json");

        programmingTools
                = mapper.readValue(new File(programmingToolsUrl.toURI()), new TypeReference<Set<String>>() {
        });

        this.mapper = mapper;

    }

    @Override
    public Map<String, String> detect(final List<String> words) {
        return null;
    }
}
