package com.kapango.jobstash.matchers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class Country {

    private String name;
    private String iso2;
    private String iso3;
    private String region;
    private String countryCode;
}