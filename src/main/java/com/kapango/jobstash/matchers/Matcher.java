package com.kapango.jobstash.matchers;

import java.util.List;

public interface Matcher<T> {

    List<T> match(List<String> words);
}
