package com.kapango.jobstash.matchers;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class StateCountry extends NamedEntity {

    private final HashMap<String, String> stateCountryMap;
    private final List<CountryStates> countryStatesList;

    public StateCountry(final ObjectMapper mapper) throws URISyntaxException, IOException {

        final URL programmingLanguagesUrl = getClass().getClassLoader().getResource("json/entities/countries_states.json");
        final URL iso2StateCountryURL = getClass().getClassLoader().getResource("json/entities/us_states_iso2.json");

        stateCountryMap = new HashMap<>();

        countryStatesList
                = mapper.readValue(new File(programmingLanguagesUrl.toURI()), new TypeReference<>() {
        });

        HashMap<String, String> iso2StateCountryMap = mapper.readValue(new File(iso2StateCountryURL.toURI()), new TypeReference<>() {});

        countryStatesList.forEach(countryStates -> {
            countryStates.getStates()
                    .forEach(state -> stateCountryMap.put(state, countryStates.getCountry()));
        });

        log.info("Loaded State Country data");
    }

    @Override
    public Map<String, String> detect(final List<String> words) {
        return null;
    }

}
