package com.kapango.jobstash.matchers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.stereotype.Component;

import com.kapango.jobstash.model.token.ProgrammingFramework;
import com.kapango.jobstash.repository.token.ProgrammingFrameworkRepository;

@Component
public class ProgrammingFrameworksMatcher implements Matcher<ProgrammingFramework> {

    private final ProgrammingFrameworkRepository repository;

    public ProgrammingFrameworksMatcher(ProgrammingFrameworkRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<ProgrammingFramework> match(List<String> words) {

        var programmingFrameworks = repository.findAll();
        var matches = new HashSet<ProgrammingFramework>();

        words.forEach(word -> programmingFrameworks.forEach(pf -> {
            if (word.equalsIgnoreCase(pf.getName()) || word.equalsIgnoreCase(pf.getAlias())) {
                matches.add(pf);
            }
        }));

        return new ArrayList<>(matches);
    }
}
