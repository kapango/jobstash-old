package com.kapango.jobstash.model.token;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "programming_framework")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProgrammingFramework {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "alias")
    private String alias;

    @ManyToOne
    @JoinColumn(name = "programming_language_id")
    private ProgrammingLanguage programmingLanguage;

    @Column(name = "url")
    private String url;

    @Column(name = "description")
    private String description;

    @Column(name = "misc")
    private String misc;

    @ManyToOne
    @JoinColumn(name = "parent_id")
    private ProgrammingFramework parent;
}
