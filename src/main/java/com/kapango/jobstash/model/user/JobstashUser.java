package com.kapango.jobstash.model.user;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.kapango.jobstash.model.RoleConverter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "user")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JobstashUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "email")
    private String email;

    @Column(name = "name")
    private String name;

    @Transient
    private String profilePictureUrl;

    /**
     * Password is hashed
     */
    @Column(name = "password")
    private String password;

    @Convert(converter = RoleConverter.class)
    @Column(name = "roles")
    private Set<Role> roles = Set.of(Role.USER);

}
