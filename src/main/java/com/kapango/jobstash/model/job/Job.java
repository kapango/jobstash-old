package com.kapango.jobstash.model.job;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.kapango.jobstash.model.jobqueue.JobQueue;
import com.kapango.jobstash.model.token.ProgrammingFramework;
import com.kapango.jobstash.model.token.ProgrammingLanguage;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "job")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Job {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "organisation_id")
    private Integer organisationId;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "url")
    private String url;

    @Column(name = "currency")
    private String currency;

    @Column(name = "salary")
    private String salary;

    @Column(name = "created")
    private LocalDateTime created;

    @Column(name = "updated")
    private LocalDateTime updated;

    @OneToOne
    @JoinColumn(name = "job_queue_id")
    private JobQueue jobQueue;

    @ManyToMany(cascade = { CascadeType.MERGE })
    @JoinTable(
            name = "job_programming_languages",
            joinColumns = { @JoinColumn(name = "job_id") },
            inverseJoinColumns = { @JoinColumn(name = "programming_language_id") }
    )
    private List<ProgrammingLanguage> programmingLanguages;

    @ManyToMany(cascade = { CascadeType.MERGE })
    @JoinTable(
            name = "job_programming_frameworks",
            joinColumns = { @JoinColumn(name = "job_id") },
            inverseJoinColumns = { @JoinColumn(name = "programming_framework_id") }
    )
    private List<ProgrammingFramework> programmingFrameworks;

}
