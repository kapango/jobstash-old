package com.kapango.jobstash.constants;

public class JobTypes {

    public static final String FULLTIME = "Full-time";
    public static final String PARTTIME = "Part-time";
    public static final String CONTRACT = "Contract";
    public static final String TEMPORARY = "Temporary";
}
