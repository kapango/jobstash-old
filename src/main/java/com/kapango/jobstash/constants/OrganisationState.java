package com.kapango.jobstash.constants;

public class OrganisationState {

    public static final String CREATED = "CREATED"; //just been added to table, requires details added
    public static final String ENRICHED = "ENRICHED"; //has as many fields populated
    public static final String UPDATED = "UPDATED"; //updated
    public static final String DISSOLVED = "DISSOLVED"; //company has stopped trading
    public static final String IGNORED = "IGNORED"; //ignored by application + any jobs associated with it
}
