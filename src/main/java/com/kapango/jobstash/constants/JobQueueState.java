package com.kapango.jobstash.constants;

public class JobQueueState {

    public static String CREATED = "CREATED";
    public static String PROCESSED = "PROCESSED";
    public static String FAIL1 = "FAIL1";
    public static String FAIL2 = "FAIL2";
    public static String FAIL3 = "FAIL3";
    public static String INVALID = "INVALID";
}
