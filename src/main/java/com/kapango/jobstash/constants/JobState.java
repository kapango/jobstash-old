package com.kapango.jobstash.constants;

public class JobState {

    public static final String CREATED = "CREATED"; //just been added to table, requires details added
    public static final String UPDATED = "UPDATED"; //updated
    public static final String CLOSED = "Closed"; //company has stopped trading
    public static final String IGNORED = "IGNORED"; //ignored by application + any jobs associated with it
}
