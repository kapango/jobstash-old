package com.kapango.jobstash.repository.organisation;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kapango.jobstash.model.organisation.Organisation;

@Transactional
@Repository
public interface OrganisationRepository extends CrudRepository<Organisation, Integer> {

    Optional<Organisation> findOrganisationByName(@Param("name") String name);

    Optional<Organisation> findFirstByState(@Param("state") String organisationState);

    Optional<Organisation> findFirstByGlassdoorScoreIsNull();

    Optional<Organisation> findFirstByGlassdoorUrlIsNull();

    List<Organisation> findAll();
}
