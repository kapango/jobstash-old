package com.kapango.jobstash.repository.token;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kapango.jobstash.model.token.ProgrammingLanguage;

@Transactional
@Repository
public interface ProgrammingLanguageRepository extends CrudRepository<ProgrammingLanguage, Integer> {

    @Override
    List<ProgrammingLanguage> findAll();
}
