package com.kapango.jobstash.repository.job;

import java.io.*;

public class JobFileLogger {

    public static final String TEMP_FILE = "jobstash-post";

    /**
     * Creates a temp file in /tmp directory
     * @param value string
     * @throws IOException
     */
    public void save(String value) throws IOException {

        File temp = File.createTempFile(TEMP_FILE, ".tmp");

        //write it
        BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
        bw.write(value);
        bw.close();
    }


    public String read() throws FileNotFoundException {

        File temp = new File("/tmp/" + TEMP_FILE);
        BufferedReader br = new BufferedReader(new FileReader(temp));

        try {
            return br.readLine();
        } catch (IOException e) {
            return null;
        }

    }

}