package com.kapango.jobstash.repository.user;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kapango.jobstash.model.user.JobstashUser;

@Transactional
@Repository
public interface UserRepository extends CrudRepository<JobstashUser, Integer> {

    Optional<JobstashUser> findFirstByEmail(@Param("email") final String email);
}
