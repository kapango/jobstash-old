package com.kapango.jobstash.bootstrap;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kapango.jobstash.model.token.ProgrammingLanguage;
import com.kapango.jobstash.repository.token.ProgrammingLanguageRepository;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j(topic = "PROGRAMMINGLANGUAGES_BOOTSTRAPPER")
public class ProgrammingLanguagesBootstrapper {

    private final List<String> programmingLanguages;
    private final ProgrammingLanguageRepository repository;

    public ProgrammingLanguagesBootstrapper(final ObjectMapper mapper,
                                            final ProgrammingLanguageRepository repository) throws IOException, URISyntaxException {
        this.repository = repository;
        final URL programmingLanguagesUrl = getClass().getClassLoader().getResource("json/entities/programming_languages.json");

        programmingLanguages
                = mapper.readValue(new File(programmingLanguagesUrl.toURI()), new TypeReference<List<String>>() {
        });

        List<String> existInDatabase = repository.findAll().stream().map(pl -> pl.getName()).collect(Collectors.toList());

        programmingLanguages.removeAll(existInDatabase);
        programmingLanguages.forEach(pl -> {
            log.info("Programming Language {} does not exist in Database, adding.", pl);

            repository.save(ProgrammingLanguage.builder()
                    .name(pl)
                    .build());
        });
    }
}
