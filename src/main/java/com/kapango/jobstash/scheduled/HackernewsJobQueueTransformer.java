package com.kapango.jobstash.scheduled;

import static com.kapango.jobstash.constants.JobQueueState.INVALID;
import static com.kapango.jobstash.constants.JobQueueState.PROCESSED;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.kapango.jobstash.constants.JobQueueState;
import com.kapango.jobstash.constants.OrganisationState;
import com.kapango.jobstash.model.jobqueue.JobQueue;
import com.kapango.jobstash.model.organisation.Organisation;
import com.kapango.jobstash.parsers.HackernewsJobPostParser;
import com.kapango.jobstash.parsers.JobPostDetails;
import com.kapango.jobstash.parsers.JobPostParserException;
import com.kapango.jobstash.repository.job.JobQueueRepository;
import com.kapango.jobstash.service.job.JobService;
import com.kapango.jobstash.service.job.JobServiceImpl;
import com.kapango.jobstash.service.organisation.OrganisationNotFoundException;
import com.kapango.jobstash.service.organisation.OrganisationServiceImpl;

import lombok.extern.slf4j.Slf4j;

/**
 * Searches the most recent Hacker News Job Topic for Job Posts
 * <p>
 * This is a scheduled task managed by Quartz scheduler in Spring boot
 */
@Component
@Slf4j(topic = "HACKERNEWS_JOB_QUEUE_TRANSFORMER")
@Transactional
public class HackernewsJobQueueTransformer extends QuartzJobBean {

    public static final String JOB_NAME = "JOB_POST_TRANSFORMER";

    private final JobQueueRepository jobQueueRepository;
    private final HackernewsJobPostParser parser;
    private final OrganisationServiceImpl organisationService;
    private final JobService jobService;

    @Autowired
    public HackernewsJobQueueTransformer(final JobQueueRepository jobQueueRepository,
                                         final HackernewsJobPostParser parser,
                                         final OrganisationServiceImpl organisationService,
                                         final JobServiceImpl jobService) {
        this.jobQueueRepository = jobQueueRepository;
        this.parser = parser;
        this.organisationService = organisationService;
        this.jobService = jobService;
    }

    @Override
    protected void executeInternal(@NotNull final JobExecutionContext jobExecutionContext) {
        log.info(JOB_NAME);

        final Optional<JobQueue> result = jobQueueRepository.findOneByState(JobQueueState.CREATED);
        final List<Organisation> organisations = organisationService.findAll();

        //1. Parse and clean job post
        if (result.isPresent()) {
            final JobQueue jobQueue = result.get();
            log.info("Found a Job post with ID {}, to be transformed", jobQueue.getHackernewsTopicId());

            Organisation organisation = null;
            try {
                JobPostDetails jobPostDetails = parser.parse(jobQueue.getText());

                try {
                    organisation = organisationService.getOrganisationByName(jobPostDetails.getCompanyName());
                } catch (OrganisationNotFoundException e) {
                    log.info("Could not find {} in the Organisation table, adding it", jobPostDetails.getCompanyName());

                    var builder=  Organisation.builder()
                            .name(jobPostDetails.getCompanyName())
                            .description("TEST")
                            .state(OrganisationState.CREATED)
                            .otherLinks(jobPostDetails.getLinks().stream().map(URI::toString).collect(Collectors.toList()));

                    if (jobPostDetails.getOrganisationUrl() != null) {
                        builder.url(jobPostDetails.getOrganisationUrl().toString());
                    }

                    organisation = builder.build();

                    try {
                        organisationService.save(organisation);
                    } catch (Exception saveException) {
                        log.error("Unable to create an organisation from Job Queue item {}", jobQueue.getId(), saveException);
                    }
                }

            } catch (JobPostParserException e) {
                log.error("Unable to parse Job Queue item {}", jobQueue.getId());
                jobQueue.setState(INVALID);
                jobQueueRepository.save(jobQueue);
            }
            finally {
                jobQueue.setState(PROCESSED);
                jobQueueRepository.save(jobQueue);
            }
        }
    }
}
