package com.kapango.jobstash.controller.job;

import com.kapango.jobstash.api.job.JobApi;
import com.kapango.jobstash.api.job.JobResponse;
import com.kapango.jobstash.service.job.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class JobController implements JobApi {

  private final JobService jobService;

  @Autowired
  public JobController(final JobService jobService) {
    this.jobService = jobService;
  }

  @GetMapping("/jobs")
  @Override
  public List<String> getLatestJobIds() {
    return jobService.getLatestJobIds();
  }

  @GetMapping("/job/{jobId}")
  @Override
  public JobResponse getJobDetails(@PathVariable final String jobId) {
    return jobService.getJobDetails(jobId);
  }
}
