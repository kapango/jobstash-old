package com.kapango.jobstash.controller;

import com.kapango.jobstash.client.glassdoor.GlassDoorClientException;
import com.kapango.jobstash.client.glassdoor.GlassDoorNotFoundException;
import com.kapango.jobstash.client.glassdoor.GlassdoorCompanyResponse;
import com.kapango.jobstash.client.glassdoor.GlassdoorHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class GDController {

    private final GlassdoorHttpClient client;

    @Autowired
    public GDController(final GlassdoorHttpClient client) {
        this.client = client;
    }

    @GetMapping("/company/{name}")
    public GlassdoorCompanyResponse getCompany(@PathVariable("name") String name) throws GlassDoorClientException, GlassDoorNotFoundException {
        return client.getGlassdoorCompany(name, name.length());
    }
}
