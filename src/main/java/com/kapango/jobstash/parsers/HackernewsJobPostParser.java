package com.kapango.jobstash.parsers;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import com.kapango.jobstash.matchers.JobTypeMatcher;
import com.kapango.jobstash.matchers.ProgrammingFrameworksMatcher;
import com.kapango.jobstash.matchers.ProgrammingLanguagesMatcher;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j(topic = "HACKERNEWS_JOB_PARSER")
public class HackernewsJobPostParser implements JobPostParser {

    private final JobTypeMatcher jobTypeMatcher;
    private final ProgrammingLanguagesMatcher programmingLanguagesMatcher;
    private final ProgrammingFrameworksMatcher programmingFrameworksMatcher;

    public HackernewsJobPostParser(final JobTypeMatcher jobTypeMatcher,
                                   final ProgrammingLanguagesMatcher programmingLanguagesMatcher,
                                   final ProgrammingFrameworksMatcher programmingFrameworksMatcher) {
        this.jobTypeMatcher = jobTypeMatcher;
        this.programmingLanguagesMatcher = programmingLanguagesMatcher;
        this.programmingFrameworksMatcher = programmingFrameworksMatcher;
    }

    @Override
    public JobPostDetails parse(final String jobPost) throws JobPostParserException {

        if (StringUtils.isEmpty(jobPost) || !jobPost.contains("|")) {
            throw new JobPostParserException("Invalid text in job queue");
        }

        var builder = JobPostDetails.builder();

        final String cleansedText = Jsoup.parse(jobPost).text();

        List<String> splitString = Arrays.asList(cleansedText.split("\\|"));
        List<String> words = Arrays.asList(cleansedText.split(" "));
        builder.companyName(splitString.get(0).trim());

        List<String> jobTypeMatches = jobTypeMatcher.match(cleansedText);

        List<URI> linksInPost = findLinksInPost(jobPost);

        builder.links(linksInPost);
        builder.organisationUrl(determineMainUrl(linksInPost));

        builder.programmingLanguages(programmingLanguagesMatcher.match(words));
        builder.programmingFrameworks(programmingFrameworksMatcher.match(words));

        if (!jobTypeMatches.isEmpty()) {
            builder.jobType(jobTypeMatches.get(0));
        }

        return builder.build();
    }


    public List<URI> findLinksInPost(final String jobPost) {

        Elements retrievedLinks = Jsoup.parse(jobPost).getElementsByTag("a");
        log.info("Looking for formatted HTML links in post");
        List<URI> links = new ArrayList<>();

        if (retrievedLinks.isEmpty()) {

            if (jobPost.contains("www") || jobPost.contains("https://")) {
                log.info("Job post contains some links, lets try hunting them down");
            }

        } else {
            retrievedLinks.forEach(link -> {
                try {
                    String href = link.attr("href");
                    links.add(new URI(href));
                } catch (URISyntaxException e) {
                    log.warn("Bad URI for company", e);
                }
            });
        }
        return links;
    }

    public URI determineMainUrl(List<URI> links) {
        if (links.isEmpty()) {
            return null;
        }
        return links.get(0);
    }

}
