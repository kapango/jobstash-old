package com.kapango.jobstash.api.job;

import java.util.List;

public interface JobApi {

  /**
   * Retrieves a list of the latest Hackernews Job Ids
   * @return list
   */
  List<String> getLatestJobIds();

  JobResponse getJobDetails(String jobId);
}
