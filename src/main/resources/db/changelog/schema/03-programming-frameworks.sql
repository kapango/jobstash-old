-- liquibase formatted sql
-- changeset bruce.taylor:add-programming-frameworks

create table programming_framework
(
    id int,
    name varchar(50) not null,
    alias varchar(50) null,
    programming_language_id int not null,
    url varchar(2083) null,
    parent_id int null,
    description text null,
    misc varchar(50) null,
    constraint programming_frameworks_programming_language_id_fk
        foreign key (programming_language_id) references programming_language (id)
);

create unique index programming_frameworks_id_uindex
    on programming_framework (id);

create unique index programming_frameworks_name_uindex
    on programming_framework (name);

alter table programming_framework
    add constraint programming_frameworks_pk
        primary key (id);

alter table programming_framework modify id int auto_increment;

