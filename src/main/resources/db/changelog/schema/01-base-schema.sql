-- liquibase formatted sql
-- changeset bruce.taylor:base-schema

create table job
(
    id              int auto_increment primary key,
    organisation_id int           not null,
    title           varchar(255)  not null,
    description     text          null,
    url             varchar(2083) null,
    currency        varchar(5)    null,
    salary          varchar(10)   null,
    created         timestamp     null,
    updated         timestamp     null,
    job_queue_id    int           null,
    stale           boolean default false
);

create index job_created_index
    on job (created);

create index job_id_index
    on job (id);

create index job_organisation_id_index
    on job (organisation_id);

create table job_queue
(
    id                  int auto_increment primary key,
    hackernews_topic_id varchar(25) not null,
    hackernews_post_id  varchar(25) not null,
    state               varchar(10) null,
    origin              varchar(30) null,
    created             timestamp   not null,
    user                varchar(50) null,
    text                text        null
);

create index job_post_id_index
    on job_queue (id);

create index job_post_job_post_id_index
    on job_queue (state);

create index job_post_hackernews_topic_id_index
    on job_queue (hackernews_topic_id);

create index job_post_timestamp_index
    on job_queue (created);

create table job_topic
(
    id        int auto_increment primary key,
    topic_id  varchar(25) not null,
    timestamp timestamp   not null,
    source    varchar(20) null,
    constraint job_topic_topic_id_uindex
        unique (topic_id)
);

create index job_topic_timestamp_index
    on job_topic (timestamp);

create index job_topic_topic_id_index
    on job_topic (topic_id);

create table organisation
(
    id                 int auto_increment
        primary key,
    name               varchar(50)                   not null,
    subdivision        varchar(50)                   null,
    url                varchar(2083)                 null,
    type               varchar(20)                   null,
    description_short  varchar(250)                  null,
    description        text                          null,
    created            timestamp                     null,
    updated            int                           null,
    sectors            text                          null,
    country            varchar(50)                   null,
    region             varchar(50)                   null,
    city               varchar(100)                  null,
    full_location      varchar(150)                  null,
    allows_remote      varchar(2)                    null,
    relocation_support tinyint(1)                    null,
    visa_sponsor       tinyint(1)                    null,
    trustpilot_url     varchar(2083)                 null,
    trustpilot_score   double                        null,
    glassdoor_score    double                        null,
    glassdoor_url      varchar(2083)                 null,
    wiki_url           text                          null,
    ycombinator_intake varchar(20)                   null,
    ycombinator_url    text                          null,
    crunchbase_url     text                          null,
    linkedin_url       varchar(2083)                 null,
    facebook_url       varchar(2083)                 null,
    twitter_url        varchar(2083)                 null,
    instagram_url      varchar(2083)                 null,
    state              varchar(20) default 'CREATED' null,
    employees          int                           null,
    year_founded       int                           null,
    image_alt          varchar(500)                  null,
    image              varchar(500)                  null
);

create index organisation_employees_index
    on organisation (employees);

create index organisation_name_index
    on organisation (name);

create index organisation_state_index
    on organisation (state);

create table token_job_title
(
    id        int auto_increment,
    name      varchar(200) not null,
    parent_id int          null,
    constraint token_job_title_pk
        primary key (id)
)
    comment 'Provides a list of job titles to tokenize from job data';


create table token_job_sector
(
    id        int auto_increment,
    name      varchar(200) not null,
    parent_id int          null,
    constraint token_job_title_pk
        primary key (id)
)
    comment 'Provides a list of job sectors / industries';


create table token_job_types
(
    id        int auto_increment,
    name      varchar(200) not null,
    parent_id int          null,
    constraint token_job_title_pk
        primary key (id)
)
    comment 'Provides a list of types of jobs e.g. full time/ part';

create table token_job_location
(
    id        int auto_increment,
    name      varchar(200) not null,
    parent_id int          null,
    constraint token_job_title_pk
        primary key (id)
)
    comment 'Job remote or not, regionally restricted';


create table token_job_industries
(
    id        int auto_increment,
    name      varchar(200) not null,
    parent_id int          null,
    constraint token_job_title_pk
        primary key (id)
)
    comment 'Job remote or not, regionally restricted';

create table sector
(
    id        int auto_increment,
    name      varchar(50) not null,
    parent_id int         null,
    constraint sector_id_uindex
        unique (id),
    constraint sector_sector_id_fk
        foreign key (parent_id) references sector (id)
);

alter table sector
    add primary key (id);


create table organisation_sectors
(
    id              int auto_increment,
    sector_id       int null,
    organisation_id int not null,
    constraint organisation_sectors_id_uindex
        unique (id),
    constraint organisation_sectors_organisation_id_fk
        foreign key (organisation_id) references organisation (id),
    constraint organisation_sectors_sector_id_fk
        foreign key (sector_id) references sector (id)
);

alter table organisation_sectors
    add primary key (id);

