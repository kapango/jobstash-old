-- liquibase formatted sql
-- changeset bruce.taylor:add-many-to-many-job-programming-languages

create table job_programming_frameworks
(
    id int,
    programming_framework_id int not null,
    job_id int not null,
    constraint job_programming_frameworks_job_id_fk
        foreign key (job_id) references job (id),
    constraint job_programming_frameworks_programming_framework_id_fk
        foreign key (programming_framework_id) references programming_framework (id)
);

create unique index job_programming_frameworks_id_uindex
    on job_programming_frameworks (id);

create index job_programming_frameworks_job_id_index
    on job_programming_frameworks (job_id);

create index job_programming_frameworks_programming_framework_id_index
    on job_programming_frameworks (programming_framework_id);

alter table job_programming_frameworks
    add constraint job_programming_frameworks_pk
        primary key (id);

alter table job_programming_frameworks modify id int auto_increment;