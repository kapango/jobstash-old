package com.kapango.jobstash.service;

import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class NLPServiceTest {

    private NLPService nlpService;

    @Before
    public void setup() throws IOException {
        nlpService = new NLPService();
    }

    @Test
    public void getSentencesFromText() {
    }

    @Test
    public void getTokensFromText() {
    }

    @Test
    public void getTokensFromSentences() {
    }

    @Test
    public void testNounTags() {

        assertThat(nlpService.getTagForToken("Bruce")).isEqualTo("NNP");
        assertThat(nlpService.getTagForToken("tom")).isEqualTo("NN");
        assertThat(nlpService.getTagForToken("car")).isEqualTo("NN");

    }

    @Test
    public void getNounsFromTokensList() {

        final List<String> tokensList = ImmutableList.of("John", "was", "eaten", "by", "a", "blue", "dragon", "called", "Smaug");

        assertThat(nlpService.getNounsFromTokensList(tokensList)).contains("John", "dragon", "Smaug");
    }

    @Test
    public void sortTokensMap() {
    }
}