package com.kapango.jobstash.client.glassdoor;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

import org.junit.Before;
import org.junit.Test;

public class GlassdoorHttpClientImplTest {

    private GlassdoorHttpClientImpl glassdoorHttpClient;

    @Before
    public void setUp() {
        glassdoorHttpClient = new GlassdoorHttpClientImpl();
    }

    @Test
    public void getGlassdoorCompany_WhenFound_ThenReturnInformation() throws GlassDoorNotFoundException, GlassDoorClientException {
        final var companyName = "Shepper";
        GlassdoorCompanyResponse glassdoorCompany = glassdoorHttpClient.getGlassdoorCompany(companyName, companyName.length());
        assertThat(glassdoorCompany.getName()).isEqualToIgnoringCase(companyName);
        assertThat(glassdoorCompany.getScore()).isNotNull();
        assertThat(glassdoorCompany.getUrl()).isNotNull();
    }

    @Test
    public void getGlassdoorCompany_WhenNotFound_ThenThrowException() {
        final var companyName = "Shoppa";
        assertThatThrownBy(() -> {
            glassdoorHttpClient.getGlassdoorCompany(companyName, companyName.length());
        }).isInstanceOf(GlassDoorNotFoundException.class)
                .hasMessageContaining("Could not find company");
    }
}