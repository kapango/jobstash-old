package com.kapango.jobstash.parsers;

import static com.kapango.jobstash.constants.JobTypes.FULLTIME;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.mock;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;

import com.kapango.jobstash.matchers.JobTypeMatcher;
import com.kapango.jobstash.matchers.ProgrammingFrameworksMatcher;
import com.kapango.jobstash.matchers.ProgrammingLanguagesMatcher;

public class HackernewsJobQueueParserTest {

    private HackernewsJobPostParser parser;

    @Before
    public void setup() {
        JobTypeMatcher jobTypeMatcher = new JobTypeMatcher();
        ProgrammingLanguagesMatcher languagesMatcher = mock(ProgrammingLanguagesMatcher.class);
        ProgrammingFrameworksMatcher frameworksMatcher = mock(ProgrammingFrameworksMatcher.class);

        parser = new HackernewsJobPostParser(jobTypeMatcher, languagesMatcher, frameworksMatcher);
    }

    @Test
    public void parse_WhenInvalidHackernewsJobPost_ThenThrowException() {

        var jobPostText = "askljcsakl;fjaskl asl;kfjaslkfjas alkjqwiorqwoir";

        assertThatThrownBy(() -> {
            parser.parse(jobPostText);
        }).isInstanceOf(JobPostParserException.class)
                .hasMessageContaining("Not a valid Hackernews Job Post, missing pipes (|)");
    }

    @Test
    public void parse_WhenValidText() throws JobPostParserException {

        var jobPostText = "Blueprint.store | London, UK | Senior Node.js Full-stack developer | Remote UK (UK only) | Full time Blueprint is a venture-backed technology start-up backed by some of the best investors in the world - enabling brands and creators to sell inside SMS and WhatsApp. Blueprint is now launching to a global waitlist having been in development for 12 months. We’re looking for an ambitious Senior Software Engineer to join our tight knit development team as we move from launching with our first clients to scaling up our business and platform. Our UI is written in React and the backend built on Serverless using AWS Lambda, Node.js, DynamoDb and MySQL (Aurora). Our roadmap consists of challenging projects, including Natural Language Processing (NLP), payment processing and delivering an exceptional user experience for our clients.\n" +
                "You will work directly with our CTO and the company founders. Our engineering team is currently 5 full-stack developers, and we are looking for full-stack developers who can contribute in all areas of the platform.<p>Salary depending on experience: £60k-£90k<p><a href=\"https:&#x2F;&#x2F;jobs.blueprint.store&#x2F;senior-software-engineer-nodejs-64051&#x2F;\" rel=\"nofollow\">https:&#x2F;&#x2F;jobs.blueprint.store&#x2F;senior-software-engineer-nodejs...</a> . Apply via form or email me at al {at} blueprint.store.<p>Sorry, at this time we are not working with recruiters and are unable to offer Visa sponsorship.";

        JobPostDetails parse = parser.parse(jobPostText);


        assertThat(parse.getCompanyName()).isEqualTo("Blueprint.store");
        assertThat(parse.getJobType()).isEqualTo(FULLTIME);

//        AWS Lambda, Node.js, DynamoDb and MySQL
    }

    @Test
    public void parse_WhenValidAlternativeHTML() {

        var jobPostText = "Rosebud AI | Full Stack, QA, and Web3 Engineer | REMOTE | Part-time, Full-time | (<a href=\"https:&#x2F;&#x2F;rosebud.ai\" rel=\"nofollow\">https:&#x2F;&#x2F;rosebud.ai</a>)<p>We&#x27;re hiring for multiple engineering roles:<p>Full Stack Engineer: <a href=\"https:&#x2F;&#x2F;rosebudstartup.notion.site&#x2F;Full-Stack-Engineer-866e12ea53e24f109ab2e66c1d9fa4fb\" rel=\"nofollow\">https:&#x2F;&#x2F;rosebudstartup.notion.site&#x2F;Full-Stack-Engineer-866e1...</a><p>Web3 Engineer: <a href=\"https:&#x2F;&#x2F;rosebudstartup.notion.site&#x2F;Web3-Engineer-706913a5ac184748af38adea0095d764\" rel=\"nofollow\">https:&#x2F;&#x2F;rosebudstartup.notion.site&#x2F;Web3-Engineer-706913a5ac1...</a><p>QA Engineer (part time only): <a href=\"https:&#x2F;&#x2F;rosebudstartup.notion.site&#x2F;QA-Engineer-5d5340cf3c7a498f92696dbd7258efa0\" rel=\"nofollow\">https:&#x2F;&#x2F;rosebudstartup.notion.site&#x2F;QA-Engineer-5d5340cf3c7a4...</a><p>Rosebud.ai is building the synthetic media platform for web3. We are helping creators make composable assets for web3 world building.<p>We&#x27;re the team that built Tokkingheads (2 million IOS downloads, all organic) that allows any portrait&#x2F;photo&#x2F;face to be animated in seconds with no skill. Creators use Tokkingheads to make memes, deepfake parodies, NFTs. We’re a team of PhD researchers, creatives and engineers backed by Y Combinator with our seed led by Vinod Khosla at Khosla Ventures with participation from Balaji Srinivasan *(Former CTO Coinbase and A16Z GP), Ilya Sutskever (cofounder&#x2F;chief scientist Open AI), Kevin Lin (cofounder COO Twitch), Holly Liu Co-founder Kaba, Kun Gao (cofounder, CEO Crunchyroll), Linda Xie Founder Scalar Capital, Jon Snoddy SVP @ Walt Disney Imagineering, Jeff Dean SVP Google Research, Adam D&#x27;Angelo CEO Quora, and Lily Liu. cofounder Earn.com.";

    }

    @Test
    public void parse_WhenValidExtractAllLinks() throws JobPostParserException, URISyntaxException {

        var jobPostText = "Waitwhile (<a href=\"https:&#x2F;&#x2F;waitwhile.com\" rel=\"nofollow\">https:&#x2F;&#x2F;waitwhile.com</a>) | Backend and Frontend Developers | Stockholm, Sweden | ONSITE or REMOTE At Waitwhile, we work on tools to eliminate the 1 trillion hours that people spend waiting in lines every year. Our queue management platform is used by over 10,000 companies worldwide and has helped 100 million people enjoy a better waiting experience.\n" +
                "Waitwhile is looking for a Frontend and&#x2F;or Backend Engineer to build features, design and implement API methods, and improve the performance and reliability of our clients and systems as we rapidly scale our product and organization. We build our app using Angular, React, NodeJS, Firebase, Firestore and Linux on GCP.<p>We’re still a small cross-functional team who genuinely enjoys working together to make Waitwhile a better product and you’ll be among our first engineers, which leaves room to really shape the work you will be doing.<p>These are full time positions in Stockholm, Sweden but we are open to remote work for the right person in GMT to GMT+2.<p>More info and how to apply: <a href=\"https:&#x2F;&#x2F;careers.waitwhile.com\" rel=\"nofollow\">https:&#x2F;&#x2F;careers.waitwhile.com</a>";

        JobPostDetails result = parser.parse(jobPostText);

        URI expected = new URI("https://waitwhile.com");

        assertThat(result.getOrganisationUrl()).isEqualTo(expected);
    }
}