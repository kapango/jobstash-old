# Kazi

_"Kazi" (Kah-zee) is the Swahili word for Job._

## Overview

This is the Java service that powers the Kazi webpage.

It consists of:
 
 * MySQL database for storing data relating to Jobs and Companies.
 * Redis store for caches, pub / sub events and other ephemeral data.
 * Spring Boot framework, to power the backend.
 * Quartz for scheduling recurring tasks.
 * Kotlin functions for data ingestion from HackerNews.
 

## Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Reference doc](https://docs.spring.io/spring-cloud-gcp/docs/1.1.0.M3/reference/htmlsingle/)

### Guides
The following guides illustrates how to use certain features concretely:

* [Messaging with Redis](https://spring.io/guides/gs/messaging-redis/)
* [Samples](https://github.com/spring-cloud/spring-cloud-gcp/tree/master/spring-cloud-gcp-samples)
* [Accessing data with MySQL](https://spring.io/guides/gs/accessing-data-mysql/)



## JSON lists

https://github.com/sourcerer-io/awesome-libraries

https://github.com/jdorfman/awesome-json-datasets

https://techmap.io/api/technology/search?q=react&p=1&s=100
https://techmap.io/
https://www.seedtable.com/european-startup-rankings?locale=en
https://www.crunchbase.com/discover/organization.companies

https://github.com/public-apis/public-apis
https://github.com/n0shake/Public-APIs

https://old.reddit.com/r/aws/comments/gxl3ly/aws_services_list_in_json/

# Research

https://stackoverflow.com/questions/41196081/spacy-alternatives-in-java


1. Get Hackernews Job Topic 
2. Get Job Posts from Hackernews Topic
   1. Extract company name
      1. Enrich / fetch company data from name
         1. Fetch from YCombinator
         2. Fetch from a,b,c,x,y etc.

   2. Extract job keywords
      1. Extract job roles / types
      2. Extract technologies
      3. Extract frameworks
   3. Extract URLs